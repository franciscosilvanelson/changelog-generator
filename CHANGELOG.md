## 2.0.0-dev.2 (2022-03-29)

### Added (1 change)

- [Added a](franciscosilvanelson/changelog-generator@00a933ce3c9e86e331117b18fa574d10b38c4743) by @franciscosilvanelson ([merge request](franciscosilvanelson/changelog-generator!16))

### Removed (1 change)

- [Removed a](franciscosilvanelson/changelog-generator@3410d330dcc4a5ab15abd8e8bb0df6da14248a08) by @franciscosilvanelson

## 2.0.0-dev.1 (2022-03-29)

### Added (1 change)

- [Added a](franciscosilvanelson/changelog-generator@00a933ce3c9e86e331117b18fa574d10b38c4743) by @franciscosilvanelson ([merge request](franciscosilvanelson/changelog-generator!16))
